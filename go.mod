module gitee.com/lflxp/tcell/v2

go 1.17

require (
	github.com/gdamore/encoding v1.0.0
	github.com/lucasb-eyer/go-colorful v1.2.0
	github.com/mattn/go-runewidth v0.0.13
	golang.org/x/sys v0.0.0-20220227234510-4e6760a101f9
	golang.org/x/term v0.0.0-20210927222741-03fcf44c2211
	golang.org/x/text v0.3.7
)

require (
	github.com/briandowns/spinner v1.18.1 // indirect
	github.com/creack/pty v1.1.17 // indirect
	github.com/fatih/color v1.7.0 // indirect
	github.com/lithammer/fuzzysearch v1.1.3 // indirect
	github.com/mattn/go-colorable v0.1.2 // indirect
	github.com/mattn/go-isatty v0.0.8 // indirect
	github.com/rivo/uniseg v0.2.0 // indirect
)

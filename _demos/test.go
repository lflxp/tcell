package main

import (
	"fmt"
	"time"
)

func main() {
	for i := 0; i < 10; i++ {
		fmt.Printf("\n")
	}

	for i := 0; i < 10; i++ {
		time.Sleep(time.Second)
		fmt.Printf("\x1b[1000D")
		fmt.Printf("\x1b[10A")
		fmt.Printf("1\n")
		fmt.Printf("2\n")
		fmt.Printf("3\n")
		fmt.Printf("4\n")
		fmt.Printf("5\n")
		fmt.Printf("6\n")
		fmt.Printf("7\n")
		fmt.Printf("8\n")
		fmt.Printf("9\n")
		fmt.Printf("10\n")
		fmt.Printf(time.Now().Format("2006-01-02 15:04:05"))
	}

	fmt.Printf("\x1b[11A")
	fmt.Printf("\x1b[J")
}
